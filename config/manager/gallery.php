<?php

$pages['index'] = [
    'isSeo' => FALSE,
    'sectionName' => 'Gallery',
    'sections' => [
        'titulo' => [
            'element' => 'inputText',
            'label' => 'Título',
            'required' => true,
        ],
        'longTitle' => [
            'element' => 'inputText',
            'label' => 'Título Largo',
            'required' => true,
        ],
        'contenido' => [
            'element' => 'inputTextarea',
            'label' => 'Contenido',
        ],
        'category' => [
            'element' => 'inputText',
            'label' => 'Categoria',
            'required' => true,
            'dataServiceCategories' => 'manager.gallery.categories', //active autocomplete for categories
            'id' => 'categoria'
        ],
        'imagen' => [
            'element' => 'imageResize',
            'label' => 'Imagen',
            'required' => true,
            'w' => '700',
            'h' => '700',
            'resize' => 'widen',
            'path' => 'img/photos',
            'ratio' => ''
        ],
        'video' => [
            'element' => 'inputText',
            'label' => 'Video',
            'required' => false,
        ],
        'tags' => [
            'element' => 'inputText',
            'label' => 'Tags',
            'required' => true,
            'id'=> 'tags',
            'dataServiceTags' => 'manager.gallery.tags',
            'helper' => "Palabras clave separadas por ','"
        ],
        'status' => [
            'element' => 'inputSelect',
            'label' => 'Visible',
            'required' => true,
             'options' => [
                 'true' =>'Si',
                 'false' =>'No'
             ],
             'helper'=>'Si el estatus es no visible el artículo no estara visible dentro del sitio'
        ],
        'locale' => [
            'element' => 'inputSelect',
            'label' => 'Idioma',
            'required' => true,
        ],
        
        
    ]
];

return $pages;

