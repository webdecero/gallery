<?php

namespace Webdecero\Gallery;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;

//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class GalleryServiceProvider extends ServiceProvider {

    
    
    private $configGallery = __DIR__ . '/../config/manager/gallery.php';
    
    private $folderViews = __DIR__ . '/Manager/resources/views';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

       
//        Publishes Configuración Base
//        php artisan vendor:publish --provider="Webdecero\Gallery\GalleryServiceProvider" --tag=config

        $this->publishes([
            $this->configGallery => config_path('manager/gallery.php'),
            
                ], 'config');

//        Registra Views
        $this->loadViewsFrom($this->folderViews, 'baseViews');

//ROUTE GALLERY 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Gallery\Manager\Controllers';
        
        $originalMiddleware =$config['middleware'];

 
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/gallery.php');
        });
        

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {

        $this->mergeConfigFrom($this->configGallery, 'manager.gallery');

//        $this->app->make('Webdecero\Gallery\Manager\Controllers\SlideController');
    }

}
