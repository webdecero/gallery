<?php

namespace Webdecero\Gallery\Manager\Models;

//use Jenssegers\Mongodb\Model as Eloquent;



use Webdecero\Base\Manager\Facades\Utilities;
use Webdecero\Base\Manager\Models\Base as Base;

class Gallery extends Base{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    

      public function __construct() {
          
          
        
//        $this->fillable = array_merge( $this->fillable,['precio']);
        
        

    }


    public function getTagsAttribute($array)
    {
        //dd($array);
        return Utilities::implodeTags($array,',');
    }


     public function getCategoryAttribute($array)
    {
        //dd($array);
        return Utilities::implodeTags($array,',');
    }


}
