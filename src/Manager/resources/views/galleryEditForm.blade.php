@extends($layout)


@section('sectionAction', 'Editar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.gallery.update',[$gallery->id]))

@section('inputs')
{{ method_field('PUT') }}

{!! $htmlInputs !!}

@endsection





