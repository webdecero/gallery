@extends($layout)


@section('sectionAction', 'Agregar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.gallery.store'))

@section('inputs')



{!! $htmlInputs !!}


@endsection