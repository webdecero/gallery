@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Galería')
@section('formAction', route('manager.gallery.dataTable'))


@section('inputs')

<div class="form-group">
	<label for="publicacion">Publicado</label> <br>

	<select class="reloadTable" name="status">
		<option value="">Ninguno</option>
		<option value="true">Publicado</option>
		<option value="false">No Publicado</option>

	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

	<div class="btn-group" role="group" aria-label="...">

		<a class="btn btn-success" href="{{ route('manager.gallery.create')}}" >
			<i class="fa fa-plus-circle"></i> NUEVO
		</a>

	</div>
	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>


@endsection




@section('dataTableColums')
<th data-details="true" ></th>


<th data-name="imagen">
	<strong>Imagen</strong>
</th>

<th data-name="titulo" data-orderable="true"  data-order="asc">
	<strong>Título</strong>
</th>


<th data-name="category" >
	<strong>Categoría</strong>
</th>
<th data-name="status" data-orderable="true">
	<strong>Publicado</strong>
</th>

<th data-name="created_at"><strong>Fecha Alta</strong></th>

<th data-name="opciones">
	<strong>Opciones</strong>
</th>

@endsection

