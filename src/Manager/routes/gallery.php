<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//gallery
Route::post('gallery/dataTable/{id?}/{excel?}', array('as' => 'manager.gallery.dataTable', 'uses' => 'GalleryDataTableController@dataTable'));
        Route::get('gallery/tags', ['as' => 'manager.gallery.tags', 'uses' => 'GalleryController@getTags']);
        Route::get('gallery/categories', ['as' => 'manager.gallery.categories', 'uses' => 'GalleryController@getCategories']);
        Route::get('gallery/{id}/delete', ['as' => 'manager.gallery.destroy', 'uses' => 'GalleryController@destroy']);
        Route::resource('gallery', 'GalleryController', ['except' => [
                'show', 'destroy'
        ]]);

        Route::resource('gallery', 'GalleryController', [
          'except' => [
            'show', 'destroy'
          ],
          'names' => [
            'index' => 'manager.gallery.index',
            'create' => 'manager.gallery.create',
            'store' => 'manager.gallery.store',
            'show' => 'manager.gallery.show',
            'edit' => 'manager.gallery.edit',
            'update' => 'manager.gallery.update',
//        'destroy' => 'manager.slide.destroy',
          ]
        ]);
