<?php

namespace Webdecero\Gallery\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
use Webdecero\Gallery\Manager\Models\Gallery;
//Helpers and Class
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Controllers\ManagerController;

use Webdecero\Base\Manager\Traits\AutocompleteTags;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;

class GalleryController extends ManagerController {

	
	use AutocompleteTags;
	use DynamicInputs;
	
	public function __construct() {
		parent::__construct();
		
		$this->autocompleteResource = new Gallery;

		$this->configPages = config('manager.gallery');
        
        
        
        foreach (\LaravelLocalization::getSupportedLocales() as $key => $value) {
            $this->arrayLocaleNative[$key] = $value['native'];
        }


	}

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		
		$this->data['user'] = Auth::user();

		return view('baseViews::galleryPanel', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		
		$this->data['user'] = Auth::user();
        $this->data['dataPage'] = [];


        $type = request('type', 'index');
        $configPage = $this->configPages[$type];


        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;


        //dd($configPage);

        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::galleryAddForm', $processData);


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$input = $request->all();


		$rules = array(
			'titulo' => array('required', 'unique:gallery,titulo'),
//			'contenido' => array('required'),
			'category' => array('required'),
			'tags' => array('required'),
			'status' => array('required')
		);


		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			return back()->withErrors($validator)->with([
						'error' => trans('baseLang::mensajes.registro.incompleto'),
					])->withInput($request->except('password'));
		} else {


			$gallery = new Gallery;

			$this->_storeGallery($request, $gallery);


			return redirect()->route("manager.gallery.index")->with([
						'mensaje' => trans('baseLang::mensajes.registro.exito'),
			]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$gallery = Gallery::findOrFail($id);
		$isValid = Utilities::idMongoValid($id);
		if (!$isValid || !isset($gallery->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }
        $this->data['user'] = Auth::user();
        $this->data['gallery'] = $gallery;
        $this->data['dataPage'] = $gallery->toArray();
        $type = request('type', 'index');
        $configPage = $this->configPages[$type];
        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;
        $processData = $this->processPageUI($configPage, $this->data);
        return view('baseViews::galleryEditForm', $processData);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$input = $request->all();
		$rules = array(
			'titulo' => array('required', 'unique:gallery,titulo'),
//			'contenido' => array('required'),
			'category' => array('required'),
			'tags' => array('required'),
			'status' => array('required')
		);
		$input['id'] = $id;
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {
			$isValid = Utilities::idMongoValid($input['id']);
            $gallery = Gallery::find($input['id']);
			if (!$isValid || !isset($gallery->id)) {
                return redirect()->route('manager.gallery.index')->with([
                            'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
                ]);
            }
			$this->_storeGallery($request, $gallery);
			return redirect()->route('manager.gallery.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		 $isValid = Utilities::idMongoValid($id);
        $gallery = Gallery::findOrFail($id);
		 if (!$isValid || !isset($gallery->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }
		$gallery->delete();
		return redirect()->route('manager.gallery.index')->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
	}

	
	 private function _storeGallery(Request $request, $gallery) {

		$input = $request->all();
		$gallery->fill($input);
		$type = request('type', 'index');
        $sections = $this->configPages[$type]['sections'];
        $gallery = $this->dynamicFillSave($gallery, $sections, $input);
		$gallery->save();
		return $gallery;
		
    }

	
}
